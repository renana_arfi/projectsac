#!/usr/bin/env node
import log, { magenta } from '@ajar/marker';
import fsp from 'fs/promises';
import fs from 'fs';
import readline from 'readline';
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const uniqeID = () => Math.random().toString(36).substring(2);

interface Note {
  "idNote": string,
  "value": string,
  "complete": boolean | string
}

// usage represents the help guide
const usage = function () {
  const usageText = `
  todo helps you manage you todo tasks.

  usage:
    todo <command>

    commands can be:

    create:      used to create a new todo + [value]
    read:        used to retrieve your todos + [all/complete/uncomplete]
    update:      used to mark a todo as complete + [id]
    remove:      used to remove a todo + [id]
    removeAc:    used to remove all completed tasks
    help:        used to print the usage guide
  `
  console.log(usageText)
}

async function init() {
  //if todo.json didnt exist create it
  if (!fs.existsSync('./todolist.json')) {
    fs.writeFileSync('./todolist.json', JSON.stringify([]))
  }
  // read todo.json 
  let data = await fsp.readFile(`./todolist.json`, 'utf-8');
  let dataNote: Note[] = JSON.parse(data);
  // creat arrNots and move there data 
  let arrNots = dataNote || [];
  // call action fun and send arrNots 
  action(arrNots);
}

function action(arrNots: Note[]) {
  const myArgs: string[] = process.argv.slice(2);

  // we make sure the length of the arguments isnt greater then 2 
  if (myArgs.length > 2) {
    errorLog(`only one or two argument can be accepted`)
    usage()
  }

  switch (myArgs[0]) {
    case 'create':
      create(arrNots, myArgs[1]);
      break;
    case 'read':
      read(arrNots, myArgs[1]);
      break;
    case 'update':
      update(arrNots, myArgs[1])
      break;
    case 'remove':
      remove(arrNots, myArgs[1])
      break;
    case 'removeAC':
      removeAllComplete(arrNots)
      break;
    case 'help':
      usage()
    default:
      errorLog('invalid command passed')
      usage()
  }
}

function prompt(question: string): Promise<string> {
  return new Promise((resolve, error) => {
    rl.question(question, answer => {
      rl.close()
      resolve(answer)
    });
  })
}

function read(arrNots: Note[], option: string) {
  switch (option) {
    case "all":
      show(arrNots);
      break;
    case "uncomplete":
      show(arrNots.filter((item) => item.complete === false))
      break;
    case "complete":
      show(arrNots.filter((item) => item.complete === true))
      break;
  }
}

async function create(arrNots: Note[], value: string) {
  if (value === undefined) {
    value = await prompt('press value note that you want to add')
  }
  // creat object 
  const newNote: Note = {
    "idNote": uniqeID(),
    "value": value,
    "complete": false
  }
  // add Note to arrNots
  arrNots.push(newNote);
  // call save fun 
  save(arrNots);
}


async function update(arrNots: Note[], idToUpdate: string) {
  if (idToUpdate === undefined) {
    idToUpdate = await prompt('press id note that you want to update');
  }

  const noteObj: undefined | Note = arrNots.find((item: Note) => item.idNote === idToUpdate)
  if (noteObj === undefined) {
    errorLog('invalid id ,check the ids that exist with show all command');
  }
  else {
    // change mod complete 
    noteObj.complete = noteObj.complete ? false : true;
    // call save fun 
    save(arrNots)
  }
}

async function remove(arrNots: Note[], idToRemove: string) {
  if (idToRemove === undefined) {
    idToRemove = await prompt('press id note that you want to remove');
  }
  
  const noteObj: undefined | Note = arrNots.find((item: Note) => item.idNote === idToRemove)
  if (noteObj === undefined) {
    errorLog('invalid id ,check the ids that exist with show all command')
  }
  else {
    // remove obj idNote from arrNots
    arrNots = arrNots.filter((item) => item.idNote !== idToRemove)
    // call save fun
    save(arrNots);
  }
}

function removeAllComplete(arrNots: Note[]) {
  //remove all complete task
  arrNots = arrNots.filter((item) => item.complete === false)
  //call save fun
  save(arrNots);
}

async function save(arrNots: Note[]) {
  // save arrNots to todo.json 
  await fsp.writeFile(`./todolist.json`, JSON.stringify(arrNots, null, 2), 'utf-8')
  show(arrNots)
}

function show(arrNots: Note[]) {
  for (const note of arrNots) {
    if (note.complete) note.complete = "\u2705";
    else note.complete = "\u274E"
  }
  console.table(arrNots);
  rl.close();
}

// used to log errors to the console in red color
function errorLog(error: string) {
  log.red(error)
}

init();